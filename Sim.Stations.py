to#Importing required libraries
import numpy as np
import matplotlib.pyplot as plt
import sys, statistics, random, itertools, csv, os
from scipy.stats import gamma
from itertools import islice

#Make new command for writing to files
def append_list(file_loc, lists):
    with open(file_loc, 'a+', newline='') as write_obj:
        csv_writer = csv.writer(write_obj)
        csv_writer.writerow(lists)

#Make new command for fractionation
def remove(l,n):
    return random.sample(l,int(len(l)*(1-n)))

#Get Input file
file = sys.argv[1]

#Open Input file and get the KS of each peak
f = open(f"{file}", 'r' ).read()
f = f.split("\n")
del f[-1]

#Start Simulations
for sim in f:
    #Prep inputs
    sim = sim.split(",")
    print(f"Working on Simulation: {sim[0]}")
    print("     WGDS:")
    peaks = []
    Simulation = sim[0]
    if sim[7] != "0":
        peaks.append([float(sim[7]),int(sim[10])])
    if sim[6] != "0":
        peaks.append([float(sim[6]),int(sim[9])])
    if sim[5] != "0":
        peaks.append([float(sim[5]),int(sim[8])])

    number = int(sim[1])
    fractionation = int(sim[2])
    here = len(peaks)-(int(sim[3]))
    speciation = float(sim[4])

    #Make Syntenic Blocks for Each Peak
    xx = 0
    totalblocks = []
    dataset_blocks = []
    for peak in peaks:
        xx=xx+1
        ks = peak[0]
        blocks= np.random.normal(loc = (ks), scale = abs(0.5*(-0.026+(.284*(ks)))) , size = number) #Get distro of mean Ks values
        ll = np.random.lognormal(2.25, abs(1.46-(float(ks)*0.75)), 500) #Make a distrbution of Block lengths in Ggenes
        length = []
        for i in ll: #remove blocks less than 4 genes long (mcscan specifications typically set to 4 genes)
            if i >= 4:
                length.append(i)
        building = []
        x=0
        for i in blocks: #For each block...
            gene = []
            x=x+1
            ll = (int((round(np.random.choice(length)))))#Pick a random length from our distribution
            totalblocks.append(ll)
            zz = []
            ll = np.random.normal(loc = (i), scale = (0.5*((0.040+ 0.362* (i)))) , size = ll) #Get distro of Ks values for the block
            for val in ll: #Make Block List
                zz.append([f"{xx}.{x}",val])
            building.extend(zz)
        dataset_blocks.append(building)

    #Make A list of CDS genes for each peak, subsiquently triplicating then fractionate each one appropriately.
    OUT_CDS = []
    dataset_cds = []
    single_cds = []
    np_cds = []
    xx = -1
    for i in peaks:
        type = int(i[1])
        print(f"            {i[0]} - {type}")
        xx = xx + 1
        CDS2 = []
        AA = []
        BB = []
        CC = []
        DD = []
        #Oldest Peak - Make genes names in Doubles or Triples depending on user input
        if xx == 0:
            x = 0
            total = len(dataset_blocks[xx]) # make a starting black list of genes equal to the amount of genes expected in the blocks
            for i in range(total): #Make 2 or 3 copies of that theoretical genes
                x=x+1
                if type == 2:
                    geneA = f"{Simulation}-{xx}_{x}G.{xx}A"
                    geneB = f"{Simulation}-{xx}_{x}G.{xx}B"
                    AA.append(geneA)
                    BB.append(geneB)

                if type == 3:
                    geneA = f"{Simulation}-{xx}_{x}G.{xx}A"
                    geneB = f"{Simulation}-{xx}_{x}G.{xx}B"
                    geneC = f"{Simulation}-{xx}_{x}G.{xx}C"
                    AA.append(geneA)
                    BB.append(geneB)
                    CC.append(geneC)


            ranger = total -1
        #Other Peaks - Make Genes names in Doubles and Triple same procedure as above, but using the previous WGD as the starting list
        else:
            total = len(CDS)
            for blob in CDS:
                if type == 2:
                    geneA = f"{blob}.{xx}A"
                    geneB = f"{blob}.{xx}B"
                    AA.append(geneA)
                    BB.append(geneB)
                if type == 3:
                    geneA = f"{blob}.{xx}A"
                    geneB = f"{blob}.{xx}B"
                    geneC = f"{blob}.{xx}C"
                    AA.append(geneA)
                    BB.append(geneB)
                    CC.append(geneC)


        #Fractionate the list of genes in each peak to avoid biasing and ensure even numbers of each retention status, do the following...
            ranger = len(AA) -1
        CDS = []
        for i in range(len(AA)):
            position = random.randint(0, ranger)
            if type == 2: #Pick random sub-genomes to keep or throw away
                order =["AA","BB"]
                choice= random.randint(0, 2)
            if type == 3:
                order =["AA","BB","CC"]
                choice= random.randint(0, 3)

            random.shuffle(order)
            order = random.sample(order,choice)

            for g in order: #Only add those which were kept to the final list of genes for that wgd
                if g == "AA":
                    CDS.append(AA[position])
                if g == "BB":
                    CDS.append(BB[position])
                if g == "CC":
                    CDS.append(CC[position])
                else:
                    pass
            if len(AA) >= 1:
                AA.pop(position)
            if len(BB) >= 1:
                BB.pop(position)
            if len(CC) >= 1:
                CC.pop(position)

            ranger = ranger - 1

        #Not all genes in a given genome origionate from WGD, make a list of these non-paleologs.
        x=0
        for x in range(round(len(CDS)*.15)):
            x=x+1
            geneA = f"{Simulation}-{xx}_{x}G.{xx}N"
            CDS.append(geneA)
        CDZ = []
        for gene in CDS:
            AB = gene[0:-1]
            CDZ.append(AB)
        CDZ = list(set(CDZ))

        #Get the final retention status of the genes and append it to a list
        datamake = []
        datasin = []
        datanp = []
        for gene1 in CDS:
            if gene1[-1] == "N":
                datanp.append([gene1])
        for gene1 in CDZ:
            tmp = []
            for gene2 in CDS:
                if gene1 == gene2[0:-1] and gene2[-1] != "N":
                    tmp.append(gene2)
            if len(tmp) == 1:
                if tmp[0][-1] == "N":
                    datanp.append(tmp)
                else:
                    datasin.append(tmp)
            else:
                datamake.append(tmp)
        dataset_cds.append(datamake)
        single_cds.append(datasin)
        np_cds.append(datanp)

        #append this list of genes to the OutGroup too!
        if xx < here or len(peaks) == 1:
            zz = int(len(CDS) * 0.9)
            OUT = random.sample(CDS,zz)
            if len(peaks) == 1:
                tmp = []
                for i in OUT:
                    tmp.append(i.rsplit('.', 1)[0])
                tmp = list(set(tmp))
                OUT_CDS.append(tmp)
            else:
                OUT = list(set(OUT))
                OUT_CDS.append(OUT)

    #Assign each double, single, and triple gene pillars to a given syntenic plot
    LENGTH = (len(dataset_cds)-1)
    final_data = []
    xx = -1
    tri = []
    dou = []
    sin = []
    single_data= []
    for i in dataset_cds:
        data_tmp = []
        xx=xx+1
        blocks = dataset_blocks[xx]
        for g in i:
            if len(blocks) > 1:

                if len(g) == 3: #Triple retained
                    if xx == here:
                        tri.append(g)
                    if len(blocks) == 0:
                            choiceA = 0
                    else: #Pick random blocks to put the genes into
                        choiceA= random.randint(0, (len(blocks)-1))
                    if len(blocks) == 0:
                            choiceB = 0
                    else:
                        choiceB= random.randint(0, (len(blocks)-1))
                    if len(blocks) == 0:
                            choiceC = 0
                    else:
                        choiceC= random.randint(0, (len(blocks)-1))

                    if choiceA == choiceB: #Making sure choices do not overlap, run past the blocks, or replace other genes
                        choiceA = choiceA-1
                    if choiceA == choiceC:
                        choiceA = choiceA-1
                    if choiceC == choiceB:
                        choiceC = choiceC-1
                    pairA = blocks[choiceA]
                    pairB = blocks[choiceB]
                    pairC = blocks[choiceC]

                    if len(blocks) >= 3:
                        tmp = [choiceA,choiceB,choiceC]
                        tmp = sorted(tmp, reverse=True)
                        if i in tmp:
                            blocks.pop(i)

                    else:
                        continue

                    bb1 = [g[0],g[1]]
                    bb2 = [g[1],g[2]]
                    bb3 = [g[0],g[2]]
                    pairA.extend(bb1) #Make the actual gene pairs for that block
                    pairB.extend(bb2)
                    pairC.extend(bb3)
                    final_data.append(pairA) #Add the gene pair to a final dataset
                    final_data.append(pairB)
                    final_data.append(pairC)

                elif len(g) == 2: #Double Retained - follow same procedure but shortened
                    if xx == here:
                        dou.append(g)
                    if len(blocks) == 0:
                        choiceA = 0
                    else:
                        choiceA = random.randint(0, (len(blocks)-1))
                    pairA = blocks[choiceA]
                    blocks.pop(choiceA)
                    bb1 = [g[0],g[1]]
                    pairA.extend(bb1)
                    final_data.append(pairA)

                else:
                    pass


    #Make some of the singles and nons in blocks, while others are alone
    z = single_cds
    single_cds = []
    final_single_data = []
    sin = []
    xx = -1
    for i in z:
        xx = xx + 1
        i2 = i
        tmp = []
        if xx == here:
            for g in range(round(len(i2)*0.10)):
                b = np.random.randint(0, len(i2))
                tmp.append(i2[b])
                sin.extend(i2[b])
                i2.pop(b)
            single_cds.append(tmp)
            for g in i2:
                final_single_data.append(g)
                sin.extend(g)
        else:
            single_cds.append(i2)



    for i in np_cds:
        for g in i:
            final_single_data.append(g)


    #Make remaining singlton and nonpaleolog syntenic pairs
    xx = -1
    for i in single_cds:
        data_tmp = []
        xx=xx+1
        blocks = dataset_blocks[xx]
        for g in i:
            if len(blocks) <= 1 or len(np_cds[xx]) <= 1:
                pass
            else:
                choiceA= random.randint(0, (len(blocks)-1))
                pairA = blocks[choiceA][0:2]
                blocks.pop(choiceA)
                choiceB = random.randint(0,(len(np_cds[xx])-1))
                geneB = np_cds[xx][choiceB]
                bb1 = [g[0],geneB[0]]
                pairA.extend(bb1)
                single_data.append(pairA)


    #Clean up old junk
    del dataset_cds
    del dataset_blocks

    #Group the genes back into blocks
    final_data_clean = []
    allpairs = 0
    track = []
    for i in final_data:
        block  = i[0]
        if block not in track:
            tmp = []
            track.append(block)
            for j in final_data:
                if block == j[0]:
                    tmp.append(j)
                    allpairs = allpairs + 1
            final_data_clean.append(tmp)

    #Clean up old junk
    del track
    del final_data


    #Split blocks back into individual pairs
    final_data_dirty = []
    for i in final_data_clean:
        for g in i:
            final_data_dirty.append(g)

    #Clean up old junk
    del final_data_clean

    #Add KS values and fractionate the OUTGROUP data a bit
    OUT_DATA = []
    xx = -1
    print("     Out Group WGDS:")
    for b in OUT_CDS:
        xx=xx+1
        if len(peaks) == 1:
                #peak = random.uniform(old,new)
                peak = speciation
                print(f"            {peak}")
                for g in b:
                    meanks = np.random.normal(loc = (peak), scale = abs(0.5*(-0.026+(.284*(peak)))), size = None)
                    kz = np.random.normal(loc = (peak), scale = (0.5*((0.040+ 0.362* (peak)))), size = None)
                    gene = [g,meanks,kz]
                    OUT_DATA.append(gene)
        else:
            if xx < (here - 1):
                peak = peaks[xx][0]
                print(f"            {peak}")
                for g in b:
                    meanks = np.random.normal(loc = (peak), scale = abs(0.5*(-0.026+(.284*(peak)))), size = None)
                    kz = np.random.normal(loc = (peak), scale = (0.5*((0.040+ 0.362* (peak)))), size = None)
                    gene = [g,meanks,kz]
                    OUT_DATA.append(gene)
            elif xx == (here - 1):
                peak = peaks[xx][0]
                print(f"            {peak}")
                for g in b:
                    meanks = np.random.normal(loc = (peak), scale = abs(0.5*(-0.026+(.284*(peak)))), size = None)
                    kz = np.random.normal(loc = (peak), scale = (0.5*((0.040+ 0.362* (peak)))), size = None)
                    gene = [g,meanks,kz]
                    OUT_DATA.append(gene)

                peak = speciation
                print(f"            {peak}")
                for g in b:
                    meanks = np.random.normal(loc = (peak), scale = abs(0.5*(-0.026+(.284*(peak)))), size = None)
                    kz = np.random.normal(loc = (peak), scale = (0.5*((0.040+ 0.362* (peak)))), size = None)
                    gene = [g,meanks,kz]
                    OUT_DATA.append(gene)
            else:
                pass

    #Add in some additional fractionation if so desired to the outgorup
    if fractionation > 0 :
        OUT_DATA = remove(OUT_DATA,fractionation)

    #Double Check and clean our simulated blocks, some quirks in the block assignment step will occasional add a single duplicate of a gene pair. It is faster to check at the end.
    z = final_data_dirty
    final_data_dirty = []
    for i in z:
        if i not in final_data_dirty:
            final_data_dirty.append(i)

    #Go through the old singles, doubles, and tripples and update their class based on fractionation, tandem duplications, and gene conversion
    lastcheck = []
    for i in final_data_dirty:
        lastcheck.append(i[2])
        lastcheck.append(i[3])

    yes = 0
    no = 0
    finsin = []
    findou = []
    fintri = []
    finsin = list(set(sin))
    for i in dou:
        tmp = []
        for g in i:
            if g in lastcheck:
                tmp.append(g)
        if len(tmp) == 2:
            findou.extend(tmp)
        elif len(tmp) == 1:
            finsin.extend(tmp)
    for i in tri:
        tmp = []
        for g in i:
            if g in lastcheck:
                tmp.append(g)
        if len(tmp) == 3:
            fintri.extend(tmp)
        elif len(tmp) == 2:
            findou.extend(tmp)
        elif len(tmp) == 1:
            finsin.extend(tmp)
    for i in sin:
        tmp = []
        for g in i:
            if g in lastcheck:
                tmp.append(g)
        if len(tmp) == 3:
            fintri.extend(tmp)
        elif len(tmp) == 2:
            findou.extend(tmp)
        elif len(tmp) == 1:
            finsin.extend(tmp)
    #Clean up old stuff
    del lastcheck
    del sin
    del dou
    del tri


    #Build final simulated self-self syntenic data in an Simulated_Data format closer to what MCScan looks like
    final_data_built = []
    track = []
    allmean = []
    for i in final_data_dirty:
        if i[0] not in track:
            pz = i[0].split(".")[0]
            ks = peaks[int(pz)-1][0]
            tmp = []
            make = (int(i[0].split(".")[0]) - 1)
            track.append(i[0])
            for g in final_data_dirty:
                if i[0] == g[0]:
                    tmp.append(g)
            kz = np.random.normal(loc = (ks), scale = abs(0.5*(-0.026+(.284*(ks)))) , size = None) #Get Mean Value.
            allmean.append(kz)
            for g in tmp:
                pair = [g[0],kz,g[1],(abs(g[1]-kz)/(0.040+ 0.362*(kz))),g[2],g[3]]
                final_data_built.append(pair)

    del final_data_dirty

    #Look at the Ks plots for the data - uncomment if desired
    # allks = []
    # for i in final_data_built:
    #     allks.append(i[2])
    #
    # print(len(totalblocks))
    # print(len(allks))
    # plt.hist(totalblocks, bins=100, range=(0,60))
    # plt.show()
    # plt.hist(allmean, bins=200, range=(0,2))
    # plt.show()
    # plt.hist(allks, bins=200, range=(0,2))
    # plt.show()

    #Write Paleolog Identification to file
    for i in finsin:
       tmp = [i,"S"]
       append_list(f'Simulated_Data/Sim.PALS.csv', tmp)
    for i in findou:
       tmp = [i,"D"]
       append_list(f'Simulated_Data/Sim.PALS.csv', tmp)
    for i in fintri:
       tmp = [i,"T"]
       append_list(f'Simulated_Data/Sim.PALS.csv', tmp)


    #Write self-self simulated syntenic data to file
    for i in final_data_built:
       append_list(f'Simulated_Data/Sim.Blocks.csv', i)

    #Write self-self simulated syntenic data to file
    for i in final_single_data:
       append_list(f'Simulated_Data/Sim.SinNons.csv', i)

    #Write self-Out simulated syntenic data to file
    for i in OUT_DATA:
       append_list(f'Simulated_Data/Sim.OutGroup.csv', i)
    print("-------------------------------------------")

print("Done!")
print("Now build final file...")

peaks = []
f = open(f"{file}", 'r' ).read()
f = f.split("\n")
del f[-1]
for i in f:
    i = i.split(",")
    tmp = []
    tmp.append(i[0])
    choice = int(i[3])
    pp = 0
    pp =  i[5:8]
    if choice == 1:
        tmp.append(i[5])
    if choice == 2:
        tmp.append(i[6])
    if choice == 3:
        tmp.append(i[7])
    tmp.append(i[4])
    tmp.extend(pp)
    peaks.append(tmp)

check = os.path.isfile("Simulated_Data/Sim.Forest.csv")
print("Checking For Completed Peaks")
track = 0
if check == True:
    f = open("Simulated_Data/Sim.Forest.csv",'r').read()
    done = []
    tt = len(peaks)
    for i in peaks:
        track = track + 1
        print(f"    Processing...{round((track/tt)*100)}%", end = '\r')
        if i[0] in f:
            done.append(i[0])
        else:
            break
else:
    done = []

if check == False:
    #Begin the Process
    names = ["gene","index","Paleolog","WGD1","WGD2","WGD3","DIF","In.Blocks","Dup.Blocks","Set.Dup.Blocks","Set.Genes","STDV.A","STDV.B","STDV.C","In.Out","Dup.Out","Set.Dup.Out","Set.Unique.Matches","STDV.Out","Set.Total.Matches"]
    append_list(f'Simulated_Data/Sim.Forest.csv', names)

passBlocks = 0
passSin = 0
passPals = 0
passOut = 0


for peak in peaks:
    if peak[0] in done:
        print(f"Simulation {peak[0]}: Previously Completed")
        with open('Simulated_Data/Sim.Blocks.csv', newline='') as f:
            reader = csv.reader(f)
            for i in itertools.islice(reader,(passBlocks+1),None):
                if peak[0] == i[4].split("-")[0]:
                    passBlocks = passBlocks + 1
                else:
                    break
        with open('Simulated_Data/Sim.SinNons.csv', newline='') as f:
            reader = csv.reader(f)
            for i in itertools.islice(reader,(passSin+1),None):
                if i[0].split("-")[0] == peak[0]:
                    passSin = passSin + 1
                else:
                    break
        with open('Simulated_Data/Sim.PALS.csv', newline='') as f:
            reader = csv.reader(f)
            for i in itertools.islice(reader,(passPals+1), None):
                if i[0].split("-")[0] == peak[0]:
                    passPals = passPals + 1
                else:
                    break
        with open('Simulated_Data/Sim.OutGroup.csv', newline='') as f:
            reader = csv.reader(f)
            for i in itertools.islice(reader,(passOut+1), None):
                if i[0].split("-")[0] == peak[0]:
                    passOut = passOut + 1
                else:
                    break
    else:
        print("\n")
        print(f"Simulation {peak[0]}:")

        #Sort Self-Self Data and make gene list to begin building the final Simulated_Data simulated data
        print(" Getting Gene List...")
        data = []
        genes = []
        A = []
        B = []
        with open('Simulated_Data/Sim.Blocks.csv', newline='') as f:
            reader = csv.reader(f)
            for i in itertools.islice(reader,(passBlocks+1), None):
                if peak[0] == i[4].split("-")[0]:
                    passBlocks = passBlocks + 1
                    genes.append(i[4])
                    genes.append(i[5])
                    data.append(i)
                    A.append(i[4])
                    B.append(i[5])
                else:
                    break
        print("     Preparing Data...")

        with open('Simulated_Data/Sim.SinNons.csv', newline='') as f:
            reader = csv.reader(f)
            for i in itertools.islice(reader,(passSin+1), None):
                if i[0].split("-")[0] == peak[0]:
                    genes.append(i[0])
                    passSin = passSin + 1
                else:
                    break

        genes = list(set(genes))


        #Get list of gene ID as paleolog or nonpaleolog
        pals = []
        with open('Simulated_Data/Sim.PALS.csv', newline='') as f:
            reader = csv.reader(f)
            for i in itertools.islice(reader,(passPals+1), None):
                if i[0].split("-")[0] == peak[0]:
                    pals.append(i)
                    passPals = passPals + 1
                else:
                    break
        #Get Self-Out comparison data ready
        out= []
        with open('Simulated_Data/Sim.OutGroup.csv', newline='') as f:
            reader = csv.reader(f)
            for i in itertools.islice(reader,(passOut+1), None):
                if i[0].split("-")[0] == peak[0]:
                    out.append(i)
                    passOut = passOut + 1
                else:
                    break

        peakin = float(peak[1])
        peakout = float(peak[2])
        WGDNUM = peak[-1]
        diff = float(peak[4])-float(peak[3])
        track = len(genes)

        xx=-1
        for g in genes:
            build = []
            xx=xx+1
            build.append(g)
            build.append(xx)
            np = 0
            #Find all instances in SELF-SYN
            for i in pals:
                if g == i[0]:
                    build.append(i[1])
                    np = np + 1
            if np < 1:
                build.append("N")
            build.extend(peak[3:])
            build.append(diff)
            tt = round((xx/track)*100,2)
            print(f"    Processing...{tt}%", end = '\r')
            z = [i for i,j in enumerate(A) if j in g]
            a = [i for i,j in enumerate(B) if j in g]
            z.extend(a)
            z = list(set(z))
            blocks = []

            for i in z:
                if i not in blocks:
                    blocks.append(data[i])

            build.append(len(blocks))

            #Prune to only blocks within WGD
            WGD = []
            for i in blocks:
                if abs(float(i[1])-peakin) <= (2*abs(-0.0262 + (0.284*peakin))):
                    WGD.append(i)
            build.append(len(WGD))

            #Find the genes in the family
            fam = []
            for i in WGD:
                fam.append(i[4])
                fam.append(i[5])
            fam = list(set(fam))
            FAM = []
            for k in fam:
                z = [i for i,j in enumerate(A) if j == k]
                a = [i for i,j in enumerate(B) if j == k]
                z.extend(a)
                z = list(set(z))
                blocks = []
                for i in z:
                    i = data[i]
                    if i not in FAM:
                        if abs(float(i[1])-peakin) <= (2*abs(-0.0262 + (0.284*peakin))):
                            FAM.append(i)
            family = []
            for i in FAM:
                family.append(i[4])
                family.append(i[5])

            family = list(set(family))
            build.append(len(FAM))
            build.append(len(family))

            #Cumulative STDV of each Gene in the family
            stdvg = []
            for i in family:
                ch = 0
                stdv = 0
                for z in FAM:
                    if z[4] == i or z[5] == i:
                        if ch == 0:
                            stdv = float(z[3])
                            ch = ch + 1
                        else:
                            stdv = stdv * float(z[3])
                stdvg.append(stdv)

            stdvg = sorted(stdvg)
            add = 3 - len(stdvg)
            ch = 0
            for i in stdvg:
                ch = ch + 1
                if ch <= 3:
                    build.append(i)
            if add > 0:
                for i in range(add):
                    build.append(0)

            #Begin OutGroup Analysis

            #In outgroup at all
            OUT = []
            id = g.rsplit('.', 1)[0]
            sinchecker = g.rsplit('.', 1)[1]
            for i in out:
                if sinchecker != "0N":
                    if i[0] == id:
                        if i not in OUT:
                            OUT.append(i)
            build.append(len(OUT))

            #In out Speciation peak
            OUTMATCH = []
            for i in OUT:
                if abs(float(i[1])-peakout) <= (2*abs(-0.0262 + (0.284*peakout))):
                    OUTMATCH.append(i[0])
            build.append(len(OUTMATCH))

            #Family in OutGroup
            OUTFAM = []
            OUTFAMMATCH = []
            for i in family:
                id = i.rsplit('.', 1)[0]
                for z in out:
                    if z[0] == id:
                        if z not in OUTFAMMATCH:
                            if abs(float(z[1])-peakout) <= (2*abs(-0.0262 + (0.284*peakout))):
                                tmp = []
                                tmp.extend(z)
                                tmp.append(i)
                                OUTFAMMATCH.append(tmp)
            build.append(len(OUTFAMMATCH))

            #How many groups have the same matching outgene
            groups = []
            for i in OUTFAMMATCH:
                groups.append(i[0])

            ping = list(set(groups))
            pairs = []
            for i in ping:
                match = -1
                for g in groups:
                    if i == g:
                        match = match + 1
                if match >= 1:
                    pairs.append(i)
            pairs = list(set(pairs))
            build.append(len(pairs))

            #STDV of Group closest to WGD peak:
            stdvg = []
            stdvga = []
            findev = []
            for i in pairs:
                stdv = []
                stdve = 0
                for z in OUTFAMMATCH:
                    if z[0] == i:
                        if z not in stdvg:
                            stdvg.append(z)
                            stdv.append(z[1])
                ch = 0
                for z in stdv:
                    neg = 1
                    z = float(z)
                    if ch == 0:
                        stdve = abs(peakout - z) / (abs(-0.0262 + (0.284*peakout)))
                        if (peakout - z) < 0:
                            neg = -1
                    else:
                        stdve = stdve * abs(peakout - z) / (abs(-0.0262 + (0.284*peakout)))
                        if (peakout - z) < 0:
                            neg = -1
                findev.append((stdve*neg))
            z = sorted(findev)
            if len(z) == 0:
                build.append("0")
            else:
                build.append(z[0])

            #How many genes involved in that group
            hit = []
            pairs = list(set(pairs))
            for i in pairs:
                for g in OUTFAMMATCH:
                    if i == g[0]:
                        hit.append(g[-1])
            hit = list(set(hit))
            build.append(len(hit))

            append_list(f'Simulated_Data/Sim.Forest.csv', build)
