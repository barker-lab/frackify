import numpy as np
import sys, statistics, random, itertools, csv

file = sys.argv[1]

#Make new commands
def _as_row(file_loc, list_of_elem):
    with open(file_loc, 'a+', newline='') as write_obj:
        csv_writer = csv.writer(write_obj)
        csv_writer.writerow(list_of_elem)

#For each species in peak file...
f = open(f"{file}",'r').read()
f = f.split("\n")
for par in f:
    par = par.split(",")
    if len(par) >= 4:
        species = par[0]
        print("\n")
        print(f"Working On: {species}")
        peakout = float(par[1])
        peakin = float(par[2])
        peakold1 = float(par[3])
        peakold2 = float(par[4])
        peaks = []
        peaks.append(peakold1)
        peaks.append(peakold2)
        peaks.append(peakin)
        peaks.sort()
        peaks = list(filter(lambda num: num != 0, peaks))
        if len(peaks) < 3:
            for i in range(3-len(peaks)):
                peaks.append(0)
        #########################
        ### SELF - SELF DATA ###
        ########################
        f = open(f"Self-Self/{species}-Self.txt", 'r' ).read()
        BLOCKS = []
        #Read MCSCANX
        if f[2] == "#":
            print("\tInput Self-Self is MCScanX File")
            f = f.split("##########################################")[1]
            f = f.split("##")
            for i in f[1:]:
                block = []
                i = i.split("\n")
                title = i[0]
                title = title.split(" ")[6]
                title = title.split("&")
                i = i[1:-1]
                for g in i:
                    tmp = []
                    g = g.split("\t")
                    tmp.extend(title)
                    tmp.extend(g[1:3])
                    tmp.append(g[-1])
                    block.append(tmp)
                BLOCKS.append(block)
        #Read Coge
        else:
            print("\tInput Self-Self is SynMap File")
            BLOCKS = []
            f = f.split("#")
            for i in f[2:]:
                if "Mean Ks:" in i:
                    pass
                else:
                    i = i.split("\n")
                    block = []
                    for g in i[1:]:
                        g = g.split("\t")
                        if len(g) > 1:
                            tmp = []
                            geneA = g[3].split("||")
                            geneB = g[7].split("||")
                            tmp.append(geneA[0])
                            tmp.append(geneB[0])
                            tmp.append(geneA[3])
                            tmp.append(geneB[3])
                            if g[0] == "undef" or g[0] =="NA":
                                tmp.append("-2")
                            else:
                                tmp.append(g[0])
                            block.append(tmp)
                    if block[0][1] <= block[0][0] and dir == "Self":
                        BLOCKS.append(block)
                    else:
                        BLOCKS.append(block)
        #Sort into data list
        print("\t\tSorting Self-Self Data")
        data = []
        x = 0
        for b in BLOCKS:
            KS = []
            for g in b:
                if float(g[-1]) > 0:
                    KS.append(float(g[-1]))
            if len(KS) > 0:
                if statistics.mean(KS) <= 6:
                    x = x + 1
                    for g in b:
                        tmp = []
                        tmp.append(x)
                        tmp.extend(g)
                        tmp.append(statistics.mean(KS))
                        if len(KS) > 1:
                            tmp.append(statistics.stdev(KS))
                            st = statistics.stdev(KS)
                        else:
                            tmp.append(0)
                            st = 0
                        if st == 0:
                            tmp.append(0)
                        else:
                            tmp.append(abs(float(g[-1])-statistics.mean(KS))/st)
                        tmp.append(len(b))
                        data.append(tmp)
        #Parse into quick queery lists
        A = []
        B = []
        for i in data:
            A.append(i[3])
            B.append(i[4])

        #########################
        ### SELF - OUT DATA ###
        ########################
        f = open(f"Self-Out/{species}-Out.txt", 'r' ).read()
        BLOCKS = []
        #Read MCSCANX
        if f[2] == "#":
            print("\tInput Self-Out is MCScanX File")
            f = f.split("##########################################")[1]
            f = f.split("##")
            for i in f[1:]:
                block = []
                i = i.split("\n")
                title = i[0]
                title = title.split(" ")[6]
                title = title.split("&")
                i = i[1:-1]
                for g in i:
                    tmp = []
                    g = g.split("\t")
                    tmp.extend(title)
                    tmp.extend(g[1:3])
                    tmp.append(g[-1])
                    block.append(tmp)
                BLOCKS.append(block)
        #Read Coge
        else:
            print("\tInput Self-Out is SynMap File")
            BLOCKS = []
            f = f.split("#")
            for i in f[2:]:
                if "Mean Ks:" in i:
                    pass
                else:
                    i = i.split("\n")
                    block = []
                    for g in i[1:]:
                        g = g.split("\t")
                        if len(g) > 1:
                            tmp = []
                            geneA = g[3].split("||")
                            geneB = g[7].split("||")
                            tmp.append(geneA[0])
                            tmp.append(geneB[0])
                            tmp.append(geneA[3])
                            tmp.append(geneB[3])
                            if g[0] == "undef" or g[0] =="NA":
                                tmp.append("-2")
                            else:
                                tmp.append(g[0])
                            block.append(tmp)
                    if block[0][1] <= block[0][0] and dir == "Self":
                        BLOCKS.append(block)
                    else:
                        BLOCKS.append(block)
        #Sort into data list
        print("\t\tSorting Self-Out Data")
        out = []
        x = 0
        for b in BLOCKS:
            KS = []
            for g in b:
                if float(g[-1]) > 0:
                    KS.append(float(g[-1]))
            if len(KS) > 0:
                if statistics.mean(KS) <= 6:
                    x = x + 1
                    for g in b:
                        tmp = []
                        tmp.append(x)
                        tmp.extend(g)
                        tmp.append(statistics.mean(KS))
                        if len(KS) > 1:
                            tmp.append(statistics.stdev(KS))
                            st = statistics.stdev(KS)
                        else:
                            tmp.append(0)
                            st = 0
                        if st == 0:
                            tmp.append(0)
                        else:
                            tmp.append(abs(float(g[-1])-statistics.mean(KS))/st)
                        tmp.append(len(b))
                        out.append(tmp)

        #####################
        ### Get All Genes ###
        #####################
        genes = []
        f = open(f"CDS/{species}.cds", 'r' ).read()
        f = f.split(">")
        del f[0]
        for i in f:
            i = i.split("\n")[0]
            genes.append(i)

        ########################
        ### MAKE FINAL FILE ###
        ########################
        print("\tMaking Forest File")
        names = ["Gene","index","WGD1","WGD2","WGD3","DIF","In.Blocks","Dup.Blocks","Set.Dup.Blocks","Set.Genes","STDV.A","STDV.B","STDV.C","In.Out","Dup.Out","Set.Dup.Out","Set.Unique.Matches","STDV.Out","Set.Total.Matches","Match.Gene"]
        
        #Make Blank Files
        with open(f"Translated_Data/{species}.Forest.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(names)
        f.close()

        genes = list(set(genes))
        track = len(genes)
        xx=-1
        for g in genes:
            build = []
            xx=xx+1
            build.append(g)
            build.append(xx)
            tt = round((xx/track)*100,2)
            for p in peaks:
                build.append(p)
            diff = float(peaks[1])-float(peaks[0])
            build.append(diff)
            print(f"\t\tProcessing...{tt}%", end = '\r')
            z = [i for i,j in enumerate(A) if j == g]
            a = [i for i,j in enumerate(B) if j == g]
            z.extend(a)
            z = list(set(z))
            blocks = []
            for i in z:
                blocks.append(data[i])
            build.append(len(blocks))

            #Prune to only blocks within WGD
            WGD = []
            for i in blocks:
                if abs(float(i[6])-peakin) <= (3*abs(-0.0262 + (0.284*peakin))):
                    WGD.append(i)
            build.append(len(WGD))

            #Find the genes in the family
            fam = []
            for i in WGD:
                fam.append(i[3])
                fam.append(i[4])
            FAM = []
            fff = []
            family = []
            for k in fam:
                z = [i for i,j in enumerate(A) if j == k]
                a = [i for i,j in enumerate(B) if j == k]
                z.extend(a)
                z = list(set(z))
                blocks = []
                for i in z:
                    i = data[i]
                    if abs(float(i[6])-peakin) <= (3*abs(-0.0262 + (0.284*peakin))):
                        if i not in FAM:
                            FAM.append(i)
                            fff.append(i[0])
                            family.append(i[3])
                            family.append(i[4])
            fff = list(set(fff))
            family = list(set(family))
            build.append(len(fff))
            build.append(len(family))

            #Cumulative STDV of each Gene in the family
            stdvg = []
            for i in family:
                ch = 0
                stdv = 0
                for z in FAM:
                    if z[3] == i or z[4] == i:
                        if ch == 0:
                            stdv = float(z[8])
                            ch = ch + 1
                        else:
                            stdv = stdv * float(z[8])
                stdvg.append(stdv)

            stdvg = sorted(stdvg)
            add = 3 - len(stdvg)
            ch = 0
            for i in stdvg:
                ch = ch + 1
                if ch <= 3:
                    build.append(i)
            if add > 0:
                for i in range(add):
                    build.append("0")

            ##Begin OutGroup Analysis##

            #In outgroup at all
            OUT = []
            for i in out:
                if i[3] == g or i[4]==g:
                    OUT.append(i)
            build.append(len(OUT))

            #In out Speciation Peak
            OUTMATCH = []
            for i in OUT:
                if abs(float(i[6])-peakout) <= (3*abs(-0.0262 + (0.284*peakout))):
                    OUTMATCH.append(i)
            build.append(len(OUTMATCH))

            #Find closest match to peak
            matcher = ["NONE"]
            tmp = 0
            for i in OUTMATCH:
                if abs(i[7]) >= tmp:
                    if g in i[3]:
                        matcher = [i[4]]
                    if g in i[4]:
                        matcher = [i[3]]
                    tmp = i[7]

            #Family in OutGroup
            OUTFAM = []
            OUTFAMMATCH = []
            for i in family:
                for z in out:
                    if z[3] == i or z[4]== i:
                        if abs(float(z[6])-peakout) <= (3*abs(-0.0262 + (0.284*peakout))):
                            OUTFAMMATCH.append(z)
            build.append(len(OUTFAMMATCH))

            #How many groups match the same outgene
            groups = []
            for i in OUTFAMMATCH:
                if i[4] not in family:
                    groups.append(i[4])
                if i[3] not in family:
                    groups.append(i[3])
            ping = list(set(groups))
            pairs = []
            for i in ping:
                match = -1
                for gr in groups:
                    if i == gr:
                        match = match + 1
                if match >= 1:
                    pairs.append(i)
            build.append(len(pairs))

            #STDV of Group closest to WGD peak:
            stdvg = []
            stdvga = []
            findev = []
            outer = ["NONE"]
            for i in pairs:
                stdv = []
                stdve = 0
                for z in OUTFAMMATCH:
                    if z[4] == i or z[3] == i:
                        if z not in stdvg:
                            stdvg.append(z)
                            stdv.append([i,z[7]])
                ch = 0
                for z in stdv:
                    neg = 1
                    outer = [z[0]]
                    check = float(z[-1])
                    if ch == 0:
                        stdve = abs(peakout - check) / abs(-0.0262 + (0.284*peakout))
                        if (peakout - check) < 0:
                            neg = -1
                            outer = [z[0]]

                    else:
                        stdve = stdve * abs(peakout - check) / abs(-0.0262 + (0.284*peakout))
                        if (peakout - check) < 0:
                            neg = -1
                            print(z)
                            outer = [z[0]]

                findev.append((stdve*neg))
            z = sorted(findev)
            if len(z) == 0:
                build.append("0")
            else:
                build.append(z[0])


            #Get the outgene if there is one`
            if matcher == ["NONE"]:
                matcher == outer

            #How many genes involved in that group
            hit = 0
            pairs = list(set(pairs))
            outer = []
            for i in pairs:
                for gr in OUTFAMMATCH:
                    if i == gr[3]:
                        outer.append(gr[4])
                        hit = hit + 1
                    if i == gr[4]:
                        outer.append(gr[3])
                        hit = hit + 1
            outer = list(set(outer))
            build.append(hit)
            build.append(matcher)

            #Get gene groups and matching out gene if there is one, we use this to match genes together at the end.
            if len(family) == 0:
                pass
            else:
                family.extend(matcher)
            if len(outer) == 0:
                outer = [g]
            outer.extend(matcher)
            _as_row(f'Translated_Data/{species}.Forest.csv', build)
            if len(family) >= 1:
                _as_row(f'Translated_Data/{species}.Groups.csv', family)
            if len(outer) >= 1:
                _as_row(f'Translated_Data/{species}.Groups.csv', outer)
