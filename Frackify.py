#Library
import pickle, csv, sys
import pandas as pd
import numpy as np
import xgboost as xgb

# disable chained assignments
pd.options.mode.chained_assignment = None 

#Make new command
def write_list(species, list):
    with open(species, 'a+', newline='') as write:
        writer = csv.writer(write)
        writer.writerow(list)

#Get input and make output
file = sys.argv[1]
species = file.split("/")[1].split(".")[0]
data = pd.read_csv(file)
#Make Blank Files
with open(f"Predictions/{species}.Groups.csv", "w") as f:
    pass
with open(f"Predictions/{species}.Class.csv", "w") as f:
    pass

print(f"Working on {species}:")

#Extract features
X_test=data[["WGD1","WGD2","WGD3", "DIF", "Set.Genes","STDV.A","STDV.B","STDV.C","Dup.Out","STDV.Out","Set.Total.Matches"]]  # Features

#Rename for model, remove if you train a new model
X_test.rename(columns = {'Set.Genes':'Family.Genes'}, inplace = True)
X_test.rename(columns = {'Set.Total.Matches':'Family.Total.Matches'}, inplace = True)

print("\tData Prep Complete...")

#Load Boosted Tree
fracify = pickle.load(open("Models/Rig.dat", "rb"))

#Test it
y_pred = fracify.predict(X_test)

#Convert to categorical predictions
def features(x):
    x = float(x)
    if x < 0.5:
        return 'N'
    elif x >= 0.5 and x < 1.5:
        return 'S'
    elif x >= 1.5 and x < 2.5:
        return 'D'
    elif x >= 2.5:
        return 'T'


y_pred = [features(i) for i in y_pred]


print("\tPredictions Complete...")

#########################################
##   Sort out all the clear cut groups ##
#########################################

#Get groups
f = open(f"Translated_Data/{species}.Groups.csv",'r').read()
f = f.split("\n")
groups = []
for i in f:
    i = i.split(",")
    groups.append(i)
del f

data["y_pred"] = y_pred
data = data.values.tolist()

leftover = []
final = []
track = len(data)
xx = 0
completed = []
singles = []
found = 0
for i in data:
    xx = xx + 1
    tt = round((xx/track)*100,2)
    print(f"\tArranging into Gene Groups...{tt}%", end = '\r')
    tmp = []
    matcher = i[-2]
    if i[-1] != "N":
        found = found + 1
        if i[-1] == "S":
            singles.append([i[-2].split("'")[1],i[0]])
        else:
            if i[0] not in completed:
                g = []
                if i[-1] == "D":
                    tracker = 3.0
                elif i[-1] == "T":
                    tracker = 4.0
                check = []
                raw = []
                out = ['NONE']
                for z in groups:
                    if f"['{i[-2]}']" == z[-1]:
                        if z[-1] != "NONE":
                            if z not in check:
                                raw.extend(z[:-1])
                                check.append(z)
                                if z[-1] != 'NONE':
                                    out = [z[-1]]
                    if i[0] in z:
                        if z not in check:
                            raw.extend(z[:-1])
                            check.append(z)
                            if z[-1] != 'NONE':
                                out = [z[-1]]


                raw = list(set(raw))
                out = list(set(out))
                out.extend(raw)
                raw = out
                if len(raw) == tracker:
                    if raw not in final:
                        final.append(raw)
                        completed.extend(raw[1:])

#add singles back to the list
for i in singles:
    if i[1] not in completed:
        completed.append(i[1])
        final.append(i)

issues = found - len(completed)
print("")
print(f"\tOut of {found} total genes, {len(completed)} matched, {issues} did not!")
print("\tWriting to Output...")
sin = []
dou = []
tri = []
for i in final:
    if len(i) == 2:
        sin.append(i)
    if len(i) == 3:
        dou.append(i)
    if len(i) == 4:
        tri.append(i)

tri.extend(dou)
tri.extend(sin)
for i in tri:
    write_list(f'Predictions/{species}.Groups.csv', i)

for i in data:
    frac = "N"
    for group in tri:
        if i[0] in group:
            z = len(group)
            if z == 2:
                frac = "S"
            if z == 3:
                frac = "D"
            if z == 4:
                frac = "T"
    tmp = [i[0],frac]

    write_list(f'Predictions/{species}.Class.csv', tmp)

