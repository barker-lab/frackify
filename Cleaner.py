#Library
import pickle, csv, sys
import pandas as pd
import numpy as np

file = sys.argv[1]
file2 = sys.argv[2]

f = open(f"{file}",'r').read()
f = f.split("\n")
f2 = open(f"{file2}",'r').read()
f2 = f2.split("\n")

clazz = file.split(".")[0]
clazz = f"{clazz}.Class.csv"


def _as_row(file_loc, list_of_elem):
    with open(file_loc, 'a+', newline='') as write_obj:
        csv_writer = csv.writer(write_obj)
        csv_writer.writerow(list_of_elem)

younger = []
edit = []
for i in f2:
    i = i.split(",")
    tmp = i[1:]
    younger.append(tmp)

for i in f:
    i = i.split(",")
    tmp = i[1:]
    if tmp in younger:
        _as_row(f"{file}_overlap.csv",i)
    else:
        _as_row(f"{file}_cleaned.csv",i)
        edit.append(i)

updated = []
for i in edit:
    i = i[1:]
    for g in i:
        if len(i) == 3:
            num = "T"
        if len(i) == 2:
            num = "D"
        if len(i) == 1:
            num = "S"
        tmp = [g,num]
        updated.append(tmp)

clz = open(f"{clazz}",'r').read()
clz = clz.split("\n")
for i in clz:
    i = i.split(",")
    tmp = i
    for z in updated:
        if i[0] == z[0]:
            tmp = z
            pass
    _as_row(f"{clazz}_cleaned.csv",tmp)
