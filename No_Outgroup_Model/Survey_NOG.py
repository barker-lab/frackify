import numpy as np
import sys, statistics, random, itertools, csv

file = sys.argv[1]

#Make new commands
def _as_row(file_loc, list_of_elem):
    with open(file_loc, 'a+', newline='') as write_obj:
        csv_writer = csv.writer(write_obj)
        csv_writer.writerow(list_of_elem)


#For each species in peak file...
f = open(f"{file}",'r').read()
f = f.split("\n")
for par in f:
    par = par.split(",")
    species = par[0]
    print("\n")
    print(f"Working On: {species}")
    peakout = float(par[1])
    peakin = float(par[2])
    peakold1 = float(par[3])
    peakold2 = float(par[4])
    peaks = []
    peaks.append(peakold1)
    peaks.append(peakold2)
    peaks.append(peakin)
    peaks.sort()
    peaks = list(filter(lambda num: num != 0, peaks))
    if len(peaks) < 3:
        for i in range(3-len(peaks)):
            peaks.append(0)
    #########################
    ### SELF - SELF DATA ###
    ########################
    f = open(f"Self-Self/{species}-Self.txt", 'r' ).read()
    BLOCKS = []
    #Read MCSCANX
    if f[2] == "#":
        print("\tInput Self-Self is MCScanX File")
        f = f.split("##########################################")[1]
        f = f.split("##")
        for i in f[1:]:
            block = []
            i = i.split("\n")
            title = i[0]
            title = title.split(" ")[6]
            title = title.split("&")
            i = i[1:-1]
            for g in i:
                tmp = []
                g = g.split("\t")
                tmp.extend(title)
                tmp.extend(g[1:3])
                tmp.append(g[-1])
                block.append(tmp)
            BLOCKS.append(block)
    #Read Coge
    else:
        print("\tInput Self-Self is SynMap File")
        BLOCKS = []
        f = f.split("#")
        for i in f[2:]:
            if "Mean Ks:" in i:
                pass
            else:
                i = i.split("\n")
                block = []
                for g in i[1:]:
                    g = g.split("\t")
                    if len(g) > 1:
                        tmp = []
                        geneA = g[3].split("||")
                        geneB = g[7].split("||")
                        tmp.append(geneA[0])
                        tmp.append(geneB[0])
                        tmp.append(geneA[3])
                        tmp.append(geneB[3])
                        if g[0] == "undef" or g[0] =="NA":
                            tmp.append("-2")
                        else:
                            tmp.append(g[0])
                        block.append(tmp)
                if block[0][1] <= block[0][0] and dir == "Self":
                    BLOCKS.append(block)
                else:
                    BLOCKS.append(block)
    #Sort into data list
    print("\t\tSorting Self-Self Data")
    data = []
    x = 0
    for b in BLOCKS:
        KS = []
        for g in b:
            if float(g[-1]) > 0:
                KS.append(float(g[-1]))
        if len(KS) > 0:
            if statistics.mean(KS) <= 6:
                x = x + 1
                for g in b:
                    tmp = []
                    tmp.append(x)
                    tmp.extend(g)
                    tmp.append(statistics.mean(KS))
                    if len(KS) > 1:
                        tmp.append(statistics.stdev(KS))
                        st = statistics.stdev(KS)
                    else:
                        tmp.append(0)
                        st = 0
                    if st == 0:
                        tmp.append(0)
                    else:
                        tmp.append(abs(float(g[-1])-statistics.mean(KS))/st)
                    tmp.append(len(b))
                    data.append(tmp)
    #Parse into quick queery lists
    A = []
    B = []
    for i in data:
        A.append(i[3])
        B.append(i[4])


    #####################
    ### Get All Genes ###
    #####################
    genes = []
    f = open(f"CDS/{species}.cds", 'r' ).read()
    f = f.split(">")
    del f[0]
    for i in f:
        i = i.split("\n")[0]
        genes.append(i)

    ########################
    ### MAKE FINAL FILE ###
    ########################
    print("\tMaking Forest File")
    names = ["Gene","index","WGD1","WGD2","WGD3","DIF","In.Blocks","Dup.Blocks","Set.Dup.Blocks","Set.Genes","STDV.A","STDV.B","STDV.C"]
    _as_row(f'Translated_Data/{species}.Forest.csv', names)

    genes = list(set(genes))
    track = len(genes)
    xx=-1
    for g in genes:
        build = []
        xx=xx+1
        build.append(g)
        build.append(xx)
        tt = round((xx/track)*100,2)
        for p in peaks:
            build.append(p)
        diff = float(peaks[1])-float(peaks[0])
        build.append(diff)
        print(f"\t\tProcessing...{tt}%", end = '\r')
        z = [i for i,j in enumerate(A) if j == g]
        a = [i for i,j in enumerate(B) if j == g]
        z.extend(a)
        z = list(set(z))
        blocks = []
        for i in z:
            blocks.append(data[i])
        build.append(len(blocks))

        #Prune to only blocks within WGD
        WGD = []
        for i in blocks:
            if abs(float(i[6])-peakin) <= (3*abs(-0.0262 + (0.284*peakin))):
                WGD.append(i)
        build.append(len(WGD))

        #Find the genes in the family
        fam = []
        for i in WGD:
            fam.append(i[3])
            fam.append(i[4])
        FAM = []
        fff = []
        family = []
        for k in fam:
            z = [i for i,j in enumerate(A) if j == k]
            a = [i for i,j in enumerate(B) if j == k]
            z.extend(a)
            z = list(set(z))
            blocks = []
            for i in z:
                i = data[i]
                if abs(float(i[6])-peakin) <= (3*abs(-0.0262 + (0.284*peakin))):
                    if i not in FAM:
                        FAM.append(i)
                        fff.append(i[0])
                        family.append(i[3])
                        family.append(i[4])
        fff = list(set(fff))
        family = list(set(family))
        build.append(len(fff))
        build.append(len(family))

        #Cumulative STDV of each Gene in the family
        stdvg = []
        for i in family:
            ch = 0
            stdv = 0
            for z in FAM:
                if z[3] == i or z[4] == i:
                    if ch == 0:
                        stdv = float(z[8])
                        ch = ch + 1
                    else:
                        stdv = stdv * float(z[8])
            stdvg.append(stdv)

        stdvg = sorted(stdvg)
        add = 3 - len(stdvg)
        ch = 0
        for i in stdvg:
            ch = ch + 1
            if ch <= 3:
                build.append(i)
        if add > 0:
            for i in range(add):
                build.append("0")

        _as_row(f'Translated_Data/{species}.Forest.csv', build)
        if len(family) >= 1:
            _as_row(f'Translated_Data/{species}.Groups.csv', family)
