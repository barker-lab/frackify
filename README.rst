Frackify
=========

:Publication: `<https://www.biorxiv.org/content/10.1101/2021.08.12.456144v1>`_

.. contents ::

Overview
---------

Frackify (Fractionation-Classify) is a boosted decision tree that uses MCScanX or CoGe SynMap output files to classify the origins of gene duplications. Specifically, Frackify identifies paleologs from a focal whole genome duplication (WGD) along with how many copies have been retained from that duplication event. In this repository we provide both a simulation framework to generate MCScanX like output files, a script to train and test new boosted decision trees, and the trained Frackify model.

Installation
-------------
Frackify is a python 3 based pipeline that requires the following python libraries: numpy (1.18.1), matplotlib (3.1.3), sys, statistics, random, itertools, csv (1.0), os, scipy.stats (1.4.1), xgboost (1.3.3), and pickle.

Directories
:::::::::::::::::::::::::::::::::::::::::::::::::

The following is the list of directories users should populate with files before running Frackify


**CDS:** This folder contains CDS fasta files for each diploid species the user intends to run Frackify on. Use the file naming format “species”.cds

**Self-Self:** For each species place either a CoGe Synmap or MCScanX output file for the self - self synteny comparison. These files should have Ka/Ks values appended to them and the gene names match the CDS file. Use the file naming formatted “species”-Self.txt

**Self-Out:** This folder will contain the  same files as described for Self-Self, however for a self - outgroup syntenic comparison. This outgroup should be a diploid closely related to the focal species while not sharing the WGD you are intending to detect paleologs from. Use the file naming format “species”-Out.txt

**In the main folder**: Supply a CSV file containing information about each species you wish to run Frackify on. This file will have 5 columns. The first will contain each species name; please make this the same as your file names. The second column will contain the mean Ks for the orthology peak in the outgroup, typically found between the focal and second oldest WGD. The 3rd - 5th columns will contain the mean Ks of the three youngest WGD in the species in ascending order. If only one WGD is present, list the others as “0”. Ex::

  Brap	0.48	0.37	0.77	1.52
  Epar	0.76	0.73	1.27	0

Main programs
--------------

Survey.py
::::::::
This python script is the first in the pipeline and will produce an intermediate file that contains the features of each gene summarized. This can be produced using the following command::

  $ python3 Survey.py “species_info_csv_file”


Running the survey.py script will populate the "Translated_Data" directory with two files. The first is "species".Forest.csv and contains the summary statistics for each gene in the CDS file. Ex::

  Gene	index	WGD1	WGD2	WGD3	DIF	In.Blocks	Dup.Blocks	Set.Dup.Blocks	Set.Genes	STDV.A	STDV.B	STDV.C	In.Out	Dup.Out	Set.Dup.Out	Set.Unique.Matches	STDV.Out	Set.Total.Matches	Match.Gene
  Bra036534	0	0.37	0.77	1.52	0.4	1	1	1	2	0.251195754304556	0.251195754304556	0	2	1	2	1	1.43367446497448	2	['AT5G25150.1']

- **WGD1, WGD2, WGD3:** Mean Ks of each WGD.
- **DIF:** Diference in terms of Ks between the focal and neighboring WGD.
- **In.Blocks:** How many syntenic blocks pairs each gene is in.	
- **Dup.Blocks:**	How many syntenic blocks pairs with mean collinear homologous gene pair Ks (HMKs) are wthin 3 st.dev of the mean WGD Ks.
- **Set.Dup.Blocks:** For the putative syntelog set, how many syntenic blocks HMKs are within 3 st.dev of mean WGD Ks?	
- **Set.Genes:** How large the WGD derived syntelog set is.
- **STDV.A, STDV.B, STDV.C:** How many standard deviations away from the mean WGD Ks the putitive WGD derived syntelogs are.
- **In.Out:** Is the each gene in a syntenic block with the out group?	
- **Dup.Out:** Here we filter down to only the syntenic blocks with HMKs within 3 standard deviations of the Orthology peak mean Ks.
- **Set.Dup.Out:** How many syntenic blocks pairs the putitive syntelog set are in with an HMKs within 3 standard deviations of the orthology peak.
- **Set.Unique.Matches:** How many genes in the outgroup multiple of the syntelogs match to.	
- **STDV.Out:** The number of standard deviations away the closeset "Set.Unique.Match" gene is to the Orthology peak mean Ks.	
- **Set.Total.Matches:** How many syntelogs in the set match to the out group gene.
- **Match.Gene:** The out group gene each syntelog matches to. 

The second file, "species".Groups.csv contains all the putative syntelog sets along with the out group gene they match to. Ex::

  Bra036534	Bra020485	AT5G25150.1
  Bra036534	Bra020485	AT5G25150.1
  Bra018365	Bra000472	AT2G29700.1

Frackify.py - Final Output
::::::::
This is the final main Frackify command and will produce predictions using the output files from Survey.py::

  $ 	python3 Frackify.py  “Translated_Data/"species".Forest.csv”

Running the main Frackify command will generate two **final output** files in the "Predictions" directory. The first, "species".Class.csv, contains the raw predictions for each gene. Ex::

  Bra036812	D
  Bra002012	T
  Bra015724	S

The second file contains the syntelog sets along with the out group gene they match to. Ex::

  AT2G16970.1	Bra002012	Bra037285	Bra013037
  AT1G26480.1	Bra024713	Bra012455	Bra016273
  AT1G21750.1	Bra017948	Bra012293	Bra016405

Inferences in Older WGD
:::::::::
Many plant species have multiple WGD in their evolutionary histories. Frackify maintains the highest degree of accuracy for the most recent WGD in a given species. When making inferences in older WGD contamination of paleologs from younger WGD may occur. If users are concerned with contamination between WGD, we recommend making inferences for both. The provided Cleaner.py script can be used to remove contaminated paleologs originating from the younger WGD from inferences in the older WGD. This script can be run using the following command::

  $ 	python3 Cleaner.py "Path_To_Focal_WGD.Groups.csv" "Path_To_Younger_WGD.Groups.csv"

Running this command will produce a new "species.Class.csv" and "species.Groups.csv" file for the focal WGD labeled "species.Class.csv_cleaned.csv" and "species.Groups.csv_cleaned.csv" respectively.

Training Additional Models
--------------------------

Sim_Stations.py
::::::::

Alternatively, users can train a new boosted decision tree using different simulations. Simulations can be produced by supplying a CSV file with a list of each desired scenario. Users will provide unique names for each scenario to be simulated in the first column. In the second column, provide the number of syntenic blocks to be simulated per WGD. The third column allows the user to specify additional fractionation from 0–1 where 0 is no additional fractionation and 1 would represent a complete loss of all paleologs. In the fourth column the user can select which WGD will be used for paleolog simulation (if there is more than one WGD in the simulation). In column 5 specify the mean Ks of the outgroup ortholog divergence. Columns 6–8 should contain the mean Ks of up to three WGD in order of ascending divergence. If less than three WGD are specified, place zeros in the remaining WGD Ks columns. The final three columns are used to identify if each WGD is a duplication (2 = paleotetraploid) or triplication (3 = paleohexaploidy). Similar to Frackify, running these simulations requires only one command.  Ex::

  $ python3 Sim_Station.py “csv_file_of_simulation_parameters”

Running the Sim_Stations script will populate the Simulated_Data folder with data from the simulation that can then be used to train new gardient boosted models. 

Frack.rig.py
::::::::

The training script has two commands, one that will generate Bayesian optimized hyperparameters for this training set (1), and  another that will replace the old trained model in the Models folder with the new model (2). Ex::

  $ python3 Frack.Rig.py “1 or 2”

This command will populate the "models" folder with a new model labeled "Rig.User.dat" if you wish to run Frackify.py using this new model please edit the script at line 25 to the path to your new model. Ex::

  25  fracify = pickle.load(open("Models/Rig.User.dat", "rb"))


Docker
------

For non-linux users it is possible to run Frackify using Docker. To do so users will first clone the Git repository and then pull the docker image via the command line::

  $ sudo docker pull mmckibben/frackify

Once complete users can then execute Frackify commands using the following::

  $ sudo docker run -u $(id -u) -v $(pwd):/home -t mmckibben/frackify python3 "Intended_Frackify_Command"

Note: Windows users should replace $(pwd) with %cd% and $(id -u) with %username%

Sources For Example Species
---------------------------
**Brassica rapa CoGe id24668** - Tang, Haibao, and Eric Lyons. Unleashing the genome of *Brassica rapa*. Frontiers in Plant Science 3 (2012): 172. https://doi.org/10.3389/fpls.2012.00172

**Eutrema parvulum CoGe id12384** - Dassanayake, Maheshi, Dong-Ha Oh, Jeffrey S. Haas, Alvaro Hernandez, Hyewon Hong, Shahjahan Ali, Dae-Jin Yun et al. The genome of the extremophile crucifer *Thellungiella parvula*. Nature Genetics 43, no. 9 (2011): 913-918. https://doi.org/10.1038/ng.889
