FROM python:3.6

COPY requirements.txt ./

RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt
    
RUN pip3 install --upgrade numpy

RUN apt-get update && apt-get -y install locales

WORKDIR /home
COPY *.py /bin/

RUN export LC_ALL=en_US.UTF-8
RUN export LANG=en_US.UTF-8
RUN locale-gen en_US.UTF-8


#CMD ["python","-u"]
