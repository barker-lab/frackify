import pandas as pd
import numpy as np
import shap, sys, pickle
import xgboost as xgb
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import label_binarize
from sklearn.model_selection import train_test_split, cross_val_score, KFold
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials, space_eval
from sklearn import metrics
import matplotlib.patches as mpatches

#Get input
function = sys.argv[1] #If you want to create the input parameters (1) or train a model (2)

#Open Data
data = pd.read_csv('Simulated_Data/Sim.Forest.csv')
X =data[["gene","WGD1","WGD2","WGD3","DIF","Set.Genes","STDV.A","STDV.B","STDV.C","Dup.Out","STDV.Out","Set.Total.Matches"]]  # Features

#Translate categorical into numeric
y = data['Paleolog']  # Labels
dict = {'N': 0,'S': 1,'D': 2,'T': 3,}
y = y.apply(lambda x: dict[x])

#Split Data for testing and training
X_train, X_test, y_train, y_test  = train_test_split(X, y, test_size=0.30, random_state = 21)

#Save genes
genes = X_test['gene']
del X_train['gene']
del X_test['gene']
del X['gene']

print("Data Prep Complete...")
#TRAIN A MODEL
if function == "2":
    #Get parmaters
    parms = open(f'Hyper.Parm.txt', 'r').read()
    parms = parms.split(",")
    parm = []
    for i in parms:
        i = i.split("=")[1]
        parm.append(i)

    #make model
    classy = xgb.XGBClassifier(
        colsample_bytree= float(parm[0]),
        gamma= int(parm[1]),
        learning_rate= float(parm[2]),
        max_depth= int(parm[3]),
        min_child_weight= int(parm[4]),
        n_estimators= int(parm[5]),
        reg_alpha= int(parm[6]),
        reg_lambda= int(parm[7]),
        subsample= float(parm[8]),
        use_label_encoder=False,
        eval_metric = 'merror')

    #Crossfold Validation
    kfold = KFold(n_splits=5)
    results = cross_val_score(classy, X, y, cv=kfold)
    print("Accuracy: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))

    #Train it
    classy.fit(X_train, y_train)

    #Save It
    pickle.dump(classy, open("Models/Rig.User.dat", "wb"))

    #Test it on our test data
    y_pred = classy.predict(X_test)

    #Make ROC
    y_score = classy.predict_proba(X_test)
    y_test_bin = label_binarize(y_test, classes=[0,1,2,3])
    n_classes = y_test_bin.shape[1]
    fpr = {}
    tpr = {}
    roc_auc = {}
    colors = ['b','g','r','c']
    b_patch = mpatches.Patch(color='b', label='Non-Paleologs')
    g_patch = mpatches.Patch(color='g', label='Single')
    r_patch = mpatches.Patch(color='r', label='Double')
    c_patch = mpatches.Patch(color='c', label='Triple')
    x=-1
    for i in range(n_classes):
        x = x + 1
        color = colors[x]
        fpr[i], tpr[i], _ = metrics.roc_curve(y_test_bin[:, i], y_score[:, i])
        plt.plot(fpr[i], tpr[i], color=f'{color}', lw=2)
        print('AUC for Class {}: {}'.format(i+1, metrics.auc(fpr[i], tpr[i])))
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic Curves')
    plt.legend(loc='lower right', frameon=False)
    plt.legend(handles=[b_patch,g_patch,r_patch,c_patch])
    plt.show()

    #Convert to categorical predictions
    def flag_feature(x):
        x = float(x)
        if x < 0.5:
            return 'N'
        elif x >= 0.5 and x < 1.5:
            return 'S'
        elif x >= 1.5 and x < 2.5:
            return 'D'
        elif x >= 2.5:
            return 'T'
    vals = y_pred
    y_pred = [flag_feature(i) for i in y_pred]
    y_test = [flag_feature(i) for i in y_test]

    #Make data for predictions and print to file
    data2 = {'Gene':genes,'KS': X_test["WGD1"],'Dif': X_test["DIF"],'y_Actual':  y_test,'y_Predicted': y_pred}
    data2 = pd.DataFrame(data2, columns=['Gene','KS','Dif','y_Actual','y_Predicted'])
    data2.to_csv(f"Testing.csv", sep='\t')
    del data2

    #Make data for SHAP values
    data = {'y_Actual':  y_test,'y_Predicted': y_pred}
    df = pd.DataFrame(data, columns=['y_Actual','y_Predicted'])

    #Making the Confusion Matrix and print heat map
    df = pd.crosstab(df['y_Actual'], df['y_Predicted'], rownames=['Predicted'], colnames=['Actual'])
    print(df)
    df['T'] = df['T'].transform(lambda x: x / x.sum())
    df['D'] = df['D'].transform(lambda x: x / x.sum())
    df['S'] = df['S'].transform(lambda x: x / x.sum())
    df['N'] = df['N'].transform(lambda x: x / x.sum())
    df = df[['N', 'S', 'D', 'T']]
    df = df.reindex(['T', 'D', 'S', 'N'])
    sns.heatmap(df, annot=True)
    plt.show()

    #SHAP prep
    explainer = shap.TreeExplainer(classy)
    shapval = explainer.shap_values(X_test)
    inter = explainer.shap_interaction_values(X_test)

    #Explain each feature in shap model
    for i in range(4):
        fig = shap.summary_plot(shapval[i], X_test,  plot_type="violin", show = False )
        plt.subplots_adjust(top=0.88, bottom=0.11, left=0.35, right=0.9, hspace=0.2, wspace=0.2)
        plt.show()
        plt.close()

#Get parameters and print to a file
elif function == "1":
    #Get parameter space
    space = {'max_depth': hp.choice('max_depth', np.arange(3, 15, 1, dtype = int)),
    'n_estimators': hp.choice('n_estimators', np.arange(50, 300, 10, dtype = int)),
    'colsample_bytree': hp.quniform('colsample_bytree', 0.5, 1.0, 0.1),
    'min_child_weight': hp.choice('min_child_weight', np.arange(0, 10, 1, dtype = int)),
    'subsample': hp.quniform('subsample', 0.5, 1.0, 0.1),
    'learning_rate': hp.quniform('learning_rate', 0.1, 0.3, 0.1),
    'gamma': hp.choice('gamma', np.arange(0, 10, 0.1, dtype = float)),
    'reg_alpha': hp.choice('reg_alpha', np.arange(0, 10, 0.1, dtype = float)),
    'reg_lambda': hp.choice('reg_lambda', np.arange(0, 20, 0.1, dtype = float)),
    'objective': 'reg:squarederror',
    'eval_metric': 'rmse'}

    #Make scoring function
    def score(params):
        booster = xgb.XGBRegressor(**params)
        score = cross_val_score(booster, X_train, y_train, scoring='neg_mean_absolute_error', cv=5).mean()
        print(-score)
        return {'loss': -score, 'status': STATUS_OK}

    #Make Trials function
    def optimization(trials, space):
        best = fmin(score, space, algo = tpe.suggest, max_evals = 200)
        return best

    #Get the best trial and then print it to Simulated_Data file
    trials = Trials()
    best_opt = optimization(trials, space)
    best_opt = str(best_opt)
    best_opt = best_opt.replace("'","")
    best_opt = best_opt.replace(":","=")
    best_opt = best_opt.replace("}","")
    best_opt = best_opt.replace("{","")
    print(best_opt)
    file = open(f'Hyper.Parm.New.txt', 'w')
    file.writelines(best_opt)
